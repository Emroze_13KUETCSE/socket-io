package com.limited.umbrellatech.socketio;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    Socket socket;

    EditText et;
    Button btnSend,btnAdd;
    TextView tv;
    String from="1",node="1_2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //http://68.183.135.207/task_manager/v1

        btnAdd = findViewById(R.id.btn_add);
        btnSend = findViewById(R.id.btn);
        et = findViewById(R.id.et);


        tv = findViewById(R.id.tv);

        try {
            socket = IO.socket("http://68.183.135.207:2020");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.out.println("only error"+e);
            Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
        }

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {

                System.out.println("-------------Connected------------");

                socket.emit("add user", "emmi "+from,from);


            }

        }).on("user joined", new Emitter.Listener() {

            @Override
            public void call(Object... args) {


                String str = args[0].toString();

                System.out.println("only io user joined : "+ str);

                //Toast.makeText(getApplicationContext(), "only io add user : "+ str, Toast.LENGTH_LONG).show();
                /*try {
                    JSONObject data = new JSONObject(str);

                    String id = data.getJSONObject("user").getString("id");
                    boolean hide_me = data.getBoolean("hide_me");

                    double lat = data.getJSONObject("user").getDouble("lat");
                    double lon = data.getJSONObject("user").getDouble("lon");




                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                //Toast.makeText(MainActivity.this, "data", Toast.LENGTH_SHORT).show();
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                String str = args[0].toString();

                System.out.println("---------------------Disconnect---------------------- "+str);
            }

        }).on("stop typing", new Emitter.Listener() {

            @Override
            public void call(Object... args) {


                String str = args[0].toString();

                System.out.println("only io stop typing : "+ str);

                /*try {
                    JSONObject data = new JSONObject(str);

                    String id = data.getJSONObject("user").getString("id");
                    boolean hide_me = data.getBoolean("hide_me");

                    double lat = data.getJSONObject("user").getDouble("lat");
                    double lon = data.getJSONObject("user").getDouble("lon");




                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                //Toast.makeText(MainActivity.this, "data", Toast.LENGTH_SHORT).show();
            }

        }).on(node, new Emitter.Listener() {

            @Override
            public void call(Object... args) {


                String str = args[0].toString();

                System.out.println("only io new sms : "+ str);


                //Toast.makeText(getApplicationContext(), "only io new sms : "+ str, Toast.LENGTH_LONG).show();
                /*try {
                    JSONObject data = new JSONObject(str);

                    String id = data.getJSONObject("user").getString("id");
                    boolean hide_me = data.getBoolean("hide_me");

                    double lat = data.getJSONObject("user").getDouble("lat");
                    double lon = data.getJSONObject("user").getDouble("lon");




                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                //Toast.makeText(MainActivity.this, "data", Toast.LENGTH_SHORT).show();
            }

        }).on("typing", new Emitter.Listener() {

            @Override
            public void call(Object... args) {


                String str = args[0].toString();

                System.out.println("only io typing : "+ str);

                /*try {
                    JSONObject data = new JSONObject(str);

                    String id = data.getJSONObject("user").getString("id");
                    boolean hide_me = data.getBoolean("hide_me");

                    double lat = data.getJSONObject("user").getDouble("lat");
                    double lon = data.getJSONObject("user").getDouble("lon");




                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                //Toast.makeText(MainActivity.this, "data", Toast.LENGTH_SHORT).show();
            }

        })
        ;
        socket.connect();

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                socket.emit("typing");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                socket.emit("stop typing");
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //socket.emit("add user", "Emmi");
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = et.getText().toString();
                socket.emit("new message", ss,node);
                et.setText("");
            }
        });
    }
}
